#Biblioteca para conexao
import socket
#Biblioteca para esperar
import time
#Biblioteca para formatação de datetime e soma de tempo
from datetime import datetime, timedelta
#Biblioteca para alterar horário em windows
import win32api

# Define Ip do Servidor e Porta
HOST = '192.168.5.192'  
PORT = 8117 # Porta referente a minha matricula 20.2.8117

conexao = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conexao.connect((HOST, PORT))


while True:
    # Recebe o horário do servidor
    string_tempo_servidor = conexao.recv(1024).decode()
    # Se existir um horário
    if len(string_tempo_servidor) > 0:
        # Recebe o horário atual do servidor
        tempo_servidor = datetime.strptime(string_tempo_servidor, '%a %b %d %H:%M:%S %Y')
        # Define o horario atual do cliente
        tempo_agora = datetime.now()
        # Calcula a diferença de tempo
        diferenca = (tempo_servidor - tempo_agora).seconds
        # variavel para incrementar tempo acrescentado
        acrescentado = 0
        
        novo_tempo = tempo_agora 
        TEMPO_PARA_AJUSTAR = 10  #segundos
        # Loop para adição gradual
        while diferenca > acrescentado:
            # Adiciona tempo para ajustar
            novo_tempo = novo_tempo + timedelta(seconds=TEMPO_PARA_AJUSTAR)
            # Atualiza horario windows
            win32api.SetSystemTime(novo_tempo.year, novo_tempo.month, novo_tempo.weekday(), novo_tempo.day, novo_tempo.hour, novo_tempo.minute, novo_tempo.second, 0)
            print(f"Horário ajustado -> {novo_tempo}")
            # Adiciona no acrescentado 
            acrescentado = acrescentado + TEMPO_PARA_AJUSTAR
            time.sleep(1)
        # Aguarda novo ajuste
        time.sleep(TEMPO_PARA_AJUSTAR)
