#Biblioteca para conexão
import socket
#Biblioteca para client ntp
import ntplib
#Biblioteca para formatar horario atual
from time import ctime

IP_HOST = '192.168.5.192'# endereço IP do servidor
PORTA = 8117  # porta referente a minha matricula
SERVER_NTP = "pool.ntp.org" # endereco do servidor ntp
cliente_ntp = ntplib.NTPClient() # Cliente ntp

# Configura socket
conexao = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Associa Host e Porta
conexao.bind((IP_HOST, PORTA))
# Escuta conexao
conexao.listen(1)
print(f"Escutando conexões em: {IP_HOST}:{PORTA}")

while True:
    cliente, endereco = conexao.accept()
    print(f"Cliente {endereco} se conectou.")
    try:
        # Faz requisição do server ntp
        resposta_servidor = cliente_ntp.request(SERVER_NTP)
        # Pega data e hora da requisição
        data_e_hora = resposta_servidor.tx_time
        # Converte a data e hora para uma string com ctime
        data_e_hora_formatada = ctime(data_e_hora)
        # Envia  para o cliente
        cliente.send(data_e_hora_formatada.encode())
    except Exception as e:
        print(f"Erro: {e}")
    finally:
        cliente.close()
